import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { map, Observable } from "rxjs";
import { environment } from "../../../environments/environment";
import { userRegister, reponseApi, userLogin, responseToken, User } from "../interfaces/auth";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  urlservice: string = environment.url;

  constructor(private _http: HttpClient) {

  }

  register(obj: userRegister): Observable<reponseApi> {
    return this._http.post<reponseApi>(this.urlservice + 'users/register/', obj, { params: { tokenheader: 'no' } });
  }

  login(obj: userLogin): Observable<responseToken> {
    return this._http.post<responseToken>(this.urlservice + 'users/access/', obj, { params: { tokenheader: 'no' } }).pipe(
      map(resp => {
        this.guardarToken(resp.token);
        this.guardarUser(resp.user);
        return resp;
      })
    );
  }

  guardarToken(token: string): void {
    localStorage.setItem('token', token);
  }

  guardarUser(user: User): void {
    localStorage.setItem('usuario', JSON.stringify(user));
  }
}
