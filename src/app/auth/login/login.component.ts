import { Component, OnInit } from '@angular/core';
import { validateBasis } from '@angular/flex-layout';
import {FormBuilder, FormGroup, Validators, FormControl} from "@angular/forms";
import { AuthService } from '../services/auth.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  hide : boolean = true;

  constructor(private _fb: FormBuilder, 
              private _service: AuthService,
              private _router: Router) { 
    this.form = this._fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, Validators.required]
    });
  }

  ngOnInit(): void {
  }

  onLogin(): void{

    if(this.form.invalid){
      this.form.markAllAsTouched();
      return;
    }    

    this._service.login(this.form.value).subscribe(
      data => {
        console.log(data);
        Swal.fire({  icon: 'success',  title: "Bienvenido"});
        this._router.navigateByUrl('admin/usuarios');
      },
      error => {
        Swal.fire({  icon: 'error',  text: error})
      }
    );
  }

  get email(): FormControl{
    return <FormControl>this.form.get('email');
  }

  get password(): FormControl{
    return <FormControl>this.form.get('password');
  }

}
