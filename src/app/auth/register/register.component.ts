import { Component, OnInit } from '@angular/core';
import { validateBasis } from '@angular/flex-layout';
import {FormBuilder, FormGroup, Validators, FormControl} from "@angular/forms";
import { AuthService } from '../services/auth.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  
  form: FormGroup;
  hide : boolean = true;

  constructor(private _fb: FormBuilder, 
              private _service: AuthService,
              private _router: Router) { 
    this.form = this._fb.group({
      username: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      last_name: [null, Validators.required],
      password: [null, Validators.required]
    });
  }

  ngOnInit(): void {
  }

  onRegister(): void{

    if(this.form.invalid){
      this.form.markAllAsTouched();
      return;
    }    

    this._service.register(this.form.value).subscribe(
      data => {
        console.log(data);
        Swal.fire({  icon: 'success',  title: "Registro correcto",  text: data.status});
        this._router.navigateByUrl('');
      },
      error => {
        Swal.fire({  icon: 'error',  text: error})
      }
    );
  }

  get username(): FormControl{
    return <FormControl>this.form.get('username');
  }

  get email(): FormControl{
    return <FormControl>this.form.get('email');
  }

  get last_name(): FormControl{
    return <FormControl>this.form.get('last_name');
  }

  get password(): FormControl{
    return <FormControl>this.form.get('password');
  }

}
