import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { map, Observable } from "rxjs";
import { environment } from "../../../environments/environment";
import { User } from '../interfaces/user';
import { reponseApi } from 'src/app/auth/interfaces/auth';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  urlservice: string = environment.url;

  constructor(private _htt: HttpClient) { }

  getUsers(): Observable<User[]>{
    return this._htt.get<User[]>(this.urlservice + 'users/crud/');
  }

  logOut(id: Number): Observable<reponseApi>{
    return this._htt.put<reponseApi>(this.urlservice + 'users/access/' + id + '/', '');
  }

}
