import { Component, OnInit } from '@angular/core';
import { User } from '../../interfaces/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  users: Array<User> = [];

  constructor(private _service: UserService) { }

  ngOnInit(): void {
    this.onGetUsers();
  }

  onGetUsers(): void{
    this._service.getUsers().subscribe(
      data => {
        this.users = data;        
      },
      error => {
        console.log(error);
      }      
    );
  }

}
