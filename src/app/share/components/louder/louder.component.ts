import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LoaderService } from '../../services/loader.service';

@Component({
  selector: 'app-louder',
  templateUrl: './louder.component.html',
  styleUrls: ['./louder.component.css']
})
export class LouderComponent implements OnInit {

  showProgress: BehaviorSubject<boolean> = this._loaderservice.isloading;

  constructor(private _loaderservice: LoaderService) { }

  ngOnInit(): void {
  }

}
